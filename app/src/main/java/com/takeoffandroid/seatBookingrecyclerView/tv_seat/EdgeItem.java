package com.takeoffandroid.seatBookingrecyclerView.tv_seat;

import com.takeoffandroid.seatBookingrecyclerView.tv_seat.AbstractItem;

public class EdgeItem extends AbstractItem {

    public EdgeItem(String label) {
        super(label);
    }


    @Override
    public int getType() {
        return TYPE_EDGE;
    }

}
