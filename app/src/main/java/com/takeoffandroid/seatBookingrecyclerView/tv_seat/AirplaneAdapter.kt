package com.takeoffandroid.seatBookingrecyclerView.tv_seat


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.takeoffandroid.seatBookingrecyclerView.R
import com.takeoffandroid.seatBookingrecyclerView.model.RowItem

class AirplaneAdapter(context: Context, items: List<RowItem>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder?>() {
    private val mOnSeatSelected: OnSeatSelected = context as OnSeatSelected

    inner class CenterViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {
        internal val imgSeatSelected: ImageView = itemView.findViewById(R.id.img_seat_selected) as ImageView
        internal val tvSeat: TextView = itemView.findViewById(R.id.tvSeat) as TextView

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val item = tvSeat.tag as RowItem
            mItems[position].isSelected = !mItems[position].isSelected
            if (mItems[position].isSelected)
                imgSeatSelected.setImageResource(R.drawable.seat_normal_selected)
            else
                imgSeatSelected.setImageResource(R.drawable.seat_normal)
        }

    }

    private class EmptyViewHolder(itemView: View?) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView!!)

    private val mContext: Context = context
    private val mLayoutInflater: LayoutInflater = LayoutInflater.from(context)
    private val mItems: List<RowItem> = items
    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return mItems[position].itemKind.getTypeView()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return if (viewType == RowItem.Kind.SEAT.getTypeView()) {
            val itemView = mLayoutInflater.inflate(R.layout.list_item_seat, parent, false)
            CenterViewHolder(itemView)
        } else {
            val itemView = View(mContext)
            EmptyViewHolder(itemView)
        }
    }


    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val rowItem = mItems[position]
        val type = rowItem.itemKind.getTypeView()
        if (type == RowItem.Kind.SEAT.getTypeView()) {
            val item = mItems[position]
            val holder = holder as CenterViewHolder
            holder.tvSeat.text = rowItem.name
            holder.tvSeat.tag = rowItem

            /* holder.imgSeatSelected.setOnClickListener {
                 //Toast.makeText(mContext, rowItem.name, Toast.LENGTH_SHORT).show()
                 mItems[position].isSelected = !mItems[position].isSelected
                 //toggleSelection(position)
                 //mOnSeatSelected.onSeatSelected(selectedItemCount)
                 Toast.makeText(mContext, mItems[position].isSelected.toString(), Toast.LENGTH_SHORT).show()
                 if (mItems[position].isSelected)
                     holder.imgSeatSelected.setImageResource(R.drawable.seat_normal_selected)
                 else
                     holder.imgSeatSelected.setImageResource(R.drawable.seat_normal)

             }*/

        }
    }

}