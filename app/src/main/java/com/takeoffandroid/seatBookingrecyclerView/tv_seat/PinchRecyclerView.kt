package com.takeoffandroid.seatBookingrecyclerView.tv_seat

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener
import android.view.View
import android.view.View.OnTouchListener


class PinchRecyclerView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : androidx.recyclerview.widget.RecyclerView(context!!, attrs, defStyleAttr), OnTouchListener {
    private var mActivePointerId = INVALID_POINTER_ID
    private var mScaleDetector: ScaleGestureDetector? = null
    private var scaleFactor = 1f
    private var maxWidth = 0.0f
    private var maxHeight = 0.0f
    private var mLastTouchX = 0f
    private var mLastTouchY = 0f
    private var posX = 0f
    private var posY = 0f
    private var width = 0f
    private var height = 0f
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        width = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        height = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        try {
            val resp = super.onInterceptTouchEvent(ev)
            Log.d(TAG, "onInterceptTouchEvent: $resp")
            return resp
        } catch (ex: IllegalArgumentException) {
            ex.printStackTrace()
        }
        return false
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        Log.d(TAG, "onTouchEvent")
        super.onTouchEvent(ev)
        val action = ev.action
        mScaleDetector!!.onTouchEvent(ev)
        when (action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                mLastTouchX = x
                mLastTouchY = y
                mActivePointerId = ev.getPointerId(0)
            }
            MotionEvent.ACTION_MOVE -> {
                val pointerIndex = (action and MotionEvent.ACTION_POINTER_INDEX_MASK
                        shr MotionEvent.ACTION_POINTER_INDEX_SHIFT)
                val x = ev.getX(pointerIndex)
                val y = ev.getY(pointerIndex)
                val dx = x - mLastTouchX
                val dy = y - mLastTouchY
                posX += dx
                posY += dy
                if (posX > 0.0f) posX = 0.0f else if (posX < maxWidth) posX = maxWidth
                if (posY > 0.0f) posY = 0.0f else if (posY < maxHeight) posY = maxHeight
                mLastTouchX = x
                mLastTouchY = y
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                mActivePointerId = INVALID_POINTER_ID
            }
            MotionEvent.ACTION_CANCEL -> {
                mActivePointerId = INVALID_POINTER_ID
            }
            MotionEvent.ACTION_POINTER_UP -> {
                val pointerIndex = action and MotionEvent.ACTION_POINTER_INDEX_MASK shr MotionEvent.ACTION_POINTER_INDEX_SHIFT
                val pointerId = ev.getPointerId(pointerIndex)
                if (pointerId == mActivePointerId) {
                    val newPointerIndex = if (pointerIndex == 0) 1 else 0
                    mLastTouchX = ev.getX(newPointerIndex)
                    mLastTouchY = ev.getY(newPointerIndex)
                    mActivePointerId = ev.getPointerId(newPointerIndex)
                }
            }
        }
        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.saveLayer(0.0f, 0.0f, getWidth().toFloat(), getHeight().toFloat(), null, Canvas.ALL_SAVE_FLAG)
        canvas.translate(posX, posY)
        canvas.scale(scaleFactor, scaleFactor)
        canvas.restore()
    }


    override fun dispatchDraw(canvas: Canvas) {
        canvas.saveLayer(0.0f, 0.0f, getWidth().toFloat(), getHeight().toFloat(), null,  Canvas.ALL_SAVE_FLAG);
        if (scaleFactor == 1.0f) {
            posX = 0.0f
            posY = 0.0f
        }
        canvas.translate(posX, posY)
        canvas.scale(scaleFactor, scaleFactor)
        super.dispatchDraw(canvas)
        canvas.restore()
        invalidate()
    }

    private inner class ScaleListener : SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            scaleFactor *= detector.scaleFactor
            scaleFactor = Math.max(1.0f, Math.min(scaleFactor, 3.0f))
            maxWidth = width - width * scaleFactor
            maxHeight = height - height * scaleFactor
            invalidate()
            return true
        }
    }

    // Map what appeared to be clicked in the potentially zoomed view back to the scale = 1 view.
    private fun getMappedView(x: Float, y: Float): View? {
        val NO_POSITION = -1
        var expectedView: View? = null
        var scanView: View
        val mappedX = (getWidth() * (x - posX) / (getWidth() * scaleFactor)).toInt()
        val mappedY = (getHeight() * (y - posY) / (getHeight() * scaleFactor)).toInt()
        var foundColumn = NO_POSITION
        var lastLeft = NO_POSITION
        // Look for the column of the expected view.
        run {
            var i = 0
            while (i < childCount && foundColumn == NO_POSITION) {
                scanView = getChildAt(i)
                val thisLeft = scanView.left
                if (mappedX <= scanView.right && mappedX >= thisLeft) {
                    foundColumn = i
                }
                if (thisLeft < lastLeft) { // Wrapped around. Touch outside of our area.
                    break
                }
                lastLeft = scanView.left
                i++
            }
        }
        if (foundColumn == NO_POSITION) {
            return null
        }
        // Find out how many columns we have.
        var colCount = foundColumn
        while (++colCount < childCount) {
            if (getChildAt(colCount).left <= lastLeft) {
                break
            }
        }
        // Look for the row.
        var i = foundColumn
        while (i < childCount) {
            scanView = getChildAt(i)
            if (mappedY >= scanView.top && mappedY <= scanView.bottom) {
                expectedView = scanView
                break
            }
            i += colCount
        }
        return expectedView
    }

    private var mClickCandidate = false
    override fun onTouch(view: View, event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> mClickCandidate = true
            MotionEvent.ACTION_MOVE -> mClickCandidate = false
            MotionEvent.ACTION_UP -> {
                if (mClickCandidate) {
                    val v = getMappedView(event.x + view.left,
                            event.y + view.top)
                    if (v != null && v.hasOnClickListeners()) {
                        v.performClick()
                    }
                    mClickCandidate = false
                }
                return true
            }
        }
        return false
    }

    companion object {
        private const val INVALID_POINTER_ID = -1
        private val TAG = PinchRecyclerView::class.java.javaClass.simpleName
    }

    init {
        setOnTouchListener(this)
        if (!isInEditMode) mScaleDetector = ScaleGestureDetector(getContext(), ScaleListener())
    }
}