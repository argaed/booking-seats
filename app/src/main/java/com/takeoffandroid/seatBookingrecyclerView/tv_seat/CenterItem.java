package com.takeoffandroid.seatBookingrecyclerView.tv_seat;

import com.takeoffandroid.seatBookingrecyclerView.tv_seat.AbstractItem;

public class CenterItem extends AbstractItem {

    public CenterItem(String label) {
        super(label);
    }


    @Override
    public int getType() {
        return TYPE_CENTER;
    }

}
