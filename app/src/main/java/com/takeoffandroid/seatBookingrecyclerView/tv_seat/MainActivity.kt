package com.takeoffandroid.seatBookingrecyclerView.tv_seat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener
import android.widget.Switch
import android.widget.TextView
import com.takeoffandroid.seatBookingrecyclerView.R
import com.takeoffandroid.seatBookingrecyclerView.model.Row
import com.takeoffandroid.seatBookingrecyclerView.model.RowItem
import kotlin.math.ceil


class MainActivity : AppCompatActivity(), OnSeatSelected {

    private var txtSeatSelected: TextView? = null

    private var mScaleGestureDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1.0f
    lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    lateinit var btnZoom: Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtSeatSelected = findViewById(R.id.txt_seat_selected) as TextView
        btnZoom = findViewById(R.id.swButton) as Switch
        mScaleGestureDetector = ScaleGestureDetector(this, ScaleListener())
        initRv(6, 6, 36, getArray())
        btnZoom.isChecked = false
        btnZoom.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("isChecked", isChecked.toString())
            if (isChecked) {
                recyclerView.isFocusable = false
                recyclerView.isClickable = false
                recyclerView.isLayoutFrozen = true
            }else{
                recyclerView.isLayoutFrozen = false
                recyclerView.isFocusable = true
                recyclerView.isClickable = true
            }
        }
    }


    private fun initRv(column: Int, row: Int, numAll: Int, array: ArrayList<RowItem>) {


        val manager = androidx.recyclerview.widget.GridLayoutManager(this, column)
        manager.spanCount = ceil(numAll / (row).toFloat().toDouble()).toInt()
        recyclerView = findViewById(R.id.lst_items)
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.layoutManager = manager
        val adapter = AirplaneAdapter(this, array)
        recyclerView.adapter = adapter
        recyclerView.isLayoutFrozen = false
    }

    override fun onSeatSelected(count: Int) {
        txtSeatSelected!!.text = "Book $count seats"
    }

    companion object {
        private const val COLUMNS = 10
    }

    fun getArray(): ArrayList<RowItem> {
        val arrayRow: ArrayList<Row> = arrayListOf()
        val ArrayRowItem: ArrayList<RowItem> = arrayListOf()
        /*val ArrayARowItem: ArrayList<RowItem> = arrayListOf()
        val ArrayBRowItem: ArrayList<RowItem> = arrayListOf()
        val ArrayCRowItem: ArrayList<RowItem> = arrayListOf()
        val ArrayDRowItem: ArrayList<RowItem> = arrayListOf()
        val ArrayERowItem: ArrayList<RowItem> = arrayListOf()*/

        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 1, "A1", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 2, "A2", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 3, "A3", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 4, "A4", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 5, "A5", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 6, "A6", false))


        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 1, "B1", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 2, "B2", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 3, "B3", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 4, "B4", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 5, "B5", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 6, "B6", false))

        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 1, "C1", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 2, "C2", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 3, "C3", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 4, "C4", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 5, "C5", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 6, "C6", true))


        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 1, "D1", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 2, "D2", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 3, "D3", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 4, "D4", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 5, "D5", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 6, "D6", false))


        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 1, "E1", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 2, "E2", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 3, "E3", false))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 4, "E4", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.EMPETY, 5, "E5", true))
        ArrayRowItem.add(RowItem(RowItem.Kind.SEAT, 6, "E6", false))

        arrayRow.add(Row(1, "A", ArrayRowItem))
        /*  arrayRow.add(Row(2, "B", ArrayARowItem))
          arrayRow.add(Row(3, "C", ArrayARowItem))
          arrayRow.add(Row(4, "D", ArrayARowItem))
          arrayRow.add(Row(5, "E", ArrayARowItem))*/

        return ArrayRowItem
    }

    override fun onTouchEvent(motionEvent: MotionEvent?): Boolean {
        mScaleGestureDetector!!.onTouchEvent(motionEvent)
        return true
    }

    inner class ScaleListener : SimpleOnScaleGestureListener() {
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {


            mScaleFactor *= scaleGestureDetector.scaleFactor
            mScaleFactor = Math.max(.1f, Math.min(mScaleFactor, 10.0f))
            Log.e("mScaleFactor", mScaleFactor.toString())
            recyclerView.scaleX = mScaleFactor
            recyclerView.scaleY = mScaleFactor
            recyclerView.isFocusable = false
            recyclerView.isClickable = false

            return true
        }
    }


}