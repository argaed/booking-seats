package com.takeoffandroid.seatBookingrecyclerView

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import com.lionsquare.seatview.Arena
import com.lionsquare.seatview.SeatType
import com.lionsquare.seatview.SeatView
import com.lionsquare.seatview.SeatViewListener
import org.json.JSONObject
import java.util.*

class SeatViewActivity : AppCompatActivity(), View.OnClickListener {
    object MY_TYPES {
        const val DISABLED_PERSON: Int = 10
        const val NUM_TICKETS = 4
    }

    lateinit var seatView: SeatView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seat_view)
        seatView = findViewById(R.id.seatView)
        val buttonNext = findViewById<Button>(R.id.buttonNext)
        val buttonUpdate = findViewById<Button>(R.id.buttonUpdate)
        buttonNext.setOnClickListener(this)
        buttonUpdate.setOnClickListener(this)
        initSeatView()
    }

    private fun initSeatView() {
        seatView.seatClickListener = object : SeatViewListener {

            override fun seatReleased(releasedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>) {

                //Toast.makeText(this@SeatViewActivity, "Released->" + releasedSeat.seatName, Toast.LENGTH_SHORT).show()
            }

            override fun seatSelected(selectedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>, selectedSeatsSize: Int): Boolean {
                if (checkAllTickets(selectedSeatsSize)) {
                    toastCsutom("Ya selecionaste todos tu asientos")
                    return false
                }
                return true
            }

            override fun canSelectSeat(clickedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>): Boolean {
                return clickedSeat.type != SeatType.TYPE.UNSELECTABLE
            }
        }
        loadJson(loadJSONFromAsset())
    }

    private fun loadJson(json: String) {
        val rowNames: HashMap<String, String> = HashMap()
        val sample = JSONObject(json)
        val rowCount = sample.getJSONObject("screen").getInt("totalRow")
        val columnCount = sample.getJSONObject("screen").getInt("totalColumn")
        seatView.initSeatView(loadSampleNew(sample, rowNames), rowCount, columnCount, rowNames)
    }

    private fun loadSampleNew(sample: JSONObject, rowNames: HashMap<String, String>): Array<Array<Arena.Seat>> {
        val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
        val arena: Arena = gson.fromJson(sample.toString(), Arena::class.java)
        val seatArray = Array(arena.screen?.totalRow!!) { Array(arena.screen?.totalColumn!!) { Arena().Seat() } }

        for (index in 0 until (arena.screen!!.rows?.size ?: 0)) {
            val rowArray: List<Arena.Row>? = arena.screen!!.rows
            val row = rowArray?.get(index)
            rowNames[row!!.rowIndex!!.toString()] = row.rowName!!

            for (indexSeat in 0 until (rowArray?.size ?: 0)) {
                val seat = rowArray?.get(index)!!.seats!!.get(indexSeat)
                seat.id = seat.name
                seat.seatName = seat.name
                seat.drawableColor = "null"
                seat.selectedDrawableColor = "null"
                //seat.drawableColor = "#4fc3f7"
                //seat.selectedDrawableColor = "#c700ff"
                seat.type = seat.type
                seat.multipleType = SeatType.MULTIPLETYPE.NOTMULTIPLE
                if (seat.multipleType == SeatType.MULTIPLETYPE.NOTMULTIPLE) {
                    seat.selectedDrawableResourceName = "circle_selected"
                    when (seat.type) {
                        SeatType.TYPE.SELECTABLE -> {
                            seat.drawableResourceName = "circle_available"
                        }
                        MY_TYPES.DISABLED_PERSON -> {
                            seat.drawableResourceName = "seat_disabledperson"
                            seat.selectedDrawableResourceName = "ic_android_24dp"
                        }
                        SeatType.TYPE.UNSELECTABLE -> {
                            seat.drawableResourceName = "circle_notavailable"
                            seat.selectedDrawableResourceName = "ic_android_24dp"
                        }
                    }
                }
                seatArray[seat.rowIndex.toInt()][seat.columnIndex.toInt()] = seat

            }
        }
        return seatArray
    }

    private fun loadJSONFromAsset(): String {
        val fileName = "sample.json"
        val jsonString = assets.open(fileName).bufferedReader().use {
            it.readText()
        }
        return jsonString
    }

    private fun loadJSONFromAssetChange(): String {
        val fileName = "sample_change.json"
        val jsonString = assets.open(fileName).bufferedReader().use {
            it.readText()
        }
        return jsonString
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.buttonNext -> {
                validAllTicketsSelect()
            }
            R.id.buttonUpdate -> {
                val rowNames: HashMap<String, String> = HashMap()
                val sample = JSONObject(loadJSONFromAssetChange())
                val rowCount = sample.getJSONObject("screen").getInt("totalRow")
                val columnCount = sample.getJSONObject("screen").getInt("totalColumn")
                seatView.updateSeat(loadSampleNew(sample, rowNames), rowCount, columnCount, rowNames)
            }
        }
    }

    private fun validAllTicketsSelect() {
        val sizeSeat = seatView.getSelectSeatSize()
        val listSelectSeat = seatView.getSelectAllSeat()
        if (sizeSeat == MY_TYPES.NUM_TICKETS) {
            //NEXT SCREEm
            toastCsutom("Completo")
            for (seat in listSelectSeat) {
                Log.e("name seat", seat.seatName)
            }
        } else {
            toastCsutom("Te falta asientos por selecionar")
        }
    }

    fun toastCsutom(messenger: String) {
        val inflater: LayoutInflater = layoutInflater
        val layout: View = inflater.inflate(R.layout.custom_toast, null)
        val text: TextView = layout.findViewById<View>(R.id.text) as TextView
        text.text = messenger

        val toast = Toast(applicationContext)
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }

    private fun checkAllTickets(selectedSeatsSize: Int): Boolean {
        return selectedSeatsSize >= MY_TYPES.NUM_TICKETS
    }

}
