package com.takeoffandroid.seatBookingrecyclerView.model

class RowItem {

    enum class Kind : TypeView {
        SEAT {
            override fun getTypeView(): Int {
                return 1
            }
        },
        EMPETY {
            override fun getTypeView(): Int {
                return 2
            }
        }
    }


    var itemKind: Kind
    var id: Int = 0
    var name: String
    var isSelected: Boolean = false

    constructor(itemKind: Kind, id: Int, name: String, isSelected: Boolean) {
        this.itemKind = itemKind
        this.id = id
        this.name = name
        this.isSelected = isSelected
    }

}