package com.takeoffandroid.seatBookingrecyclerView.model

import java.util.*

data class Row(
        var id: Int,
        var name: String,
        var items: ArrayList<RowItem>) {

}