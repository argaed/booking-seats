package com.takeoffandroid.seatBookingrecyclerView.model

interface TypeView {
    fun getTypeView(): Int
}