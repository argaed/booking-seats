package com.lionsquare.seatview


import java.util.*

interface SeatViewListener {

    fun seatReleased(releasedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>)

    fun seatSelected(selectedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>, selectedSeatsSize :Int) :Boolean

    fun canSelectSeat(clickedSeat: Arena.Seat, selectedSeats: HashMap<String, Arena.Seat>): Boolean
}