package com.lionsquare.seatview

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Arena {

    @SerializedName("screen")
    @Expose(serialize = true, deserialize = true)
    var screen: Screen? = null

    inner class Screen {
        @SerializedName("name")
        var name: String? = null

        @SerializedName("totalRow")
        @Expose(serialize = true, deserialize = true)

        var totalRow: Int? = null

        @SerializedName("totalColumn")
        @Expose(serialize = true, deserialize = true)
        var totalColumn: Int? = null

        @SerializedName("rows")
        @Expose(serialize = true, deserialize = true)

        var rows: List<Row>? = null
    }

    inner class Row {
        @SerializedName("rowName")
        @Expose(serialize = true, deserialize = true)

        var rowName: String? = null

        @SerializedName("rowIndex")
        @Expose(serialize = true, deserialize = true)
        var rowIndex: Int? = null

        @SerializedName("seats")
        @Expose(serialize = true, deserialize = true)
        var seats: List<Seat>? = null

    }

    inner class Seat {

        @SerializedName("columnIndex")
        @Expose(serialize = true, deserialize = true)
        lateinit var columnIndex: String

        @SerializedName("rowIndex")
        @Expose(serialize = true, deserialize = true)
        lateinit var rowIndex: String

        @SerializedName("name")
        @Expose(serialize = true, deserialize = true)
        lateinit var name: String

        @SerializedName("type")
        @Expose(serialize = true, deserialize = true)
        var type: Int = 0

        @SerializedName("isSelected")
        @Expose(serialize = true, deserialize = true)
        var isSelected: Boolean = false

        @SerializedName("multiple")
        @Expose(serialize = true, deserialize = true)
        var multiple: List<String>? = null

        /**
         * please provide unique id
         */
        @Expose(deserialize = false, serialize = false)
        var id: String? = null
        /**
         * if you have problem with shape,
         * please check SeatViewConfig -> seatWidthHeightRatio
         */
        @Expose(deserialize = false, serialize = false)
        var drawableResourceName: String = "null"
        @Expose(deserialize = false, serialize = false)
        var selectedDrawableResourceName: String = "null"
        @Expose(deserialize = false, serialize = false)
        var drawableColor = "null"
        @Expose(deserialize = false, serialize = false)
        var selectedDrawableColor = "null"
        @Expose(deserialize = false, serialize = false)
        var seatName = "null"
        @Expose(deserialize = false, serialize = false)
        var rowName: String? = null
        @Expose(deserialize = false, serialize = false)
        var multipleSeats: ArrayList<String> = ArrayList()

        @Expose(deserialize = false, serialize = false)
        var multipleType: Int = SeatType.MULTIPLETYPE.NOTMULTIPLE

        override fun toString(): String {
            return "Seat(columnIndex='$columnIndex', rowIndex='$rowIndex', name='$name', type=$type, isSelected=$isSelected, multiple=$multiple, id=$id, drawableResourceName='$drawableResourceName', selectedDrawableResourceName='$selectedDrawableResourceName', drawableColor='$drawableColor', selectedDrawableColor='$selectedDrawableColor', seatName='$seatName', rowName=$rowName, multipleSeats=$multipleSeats, multipleType=$multipleType)"
        }
    }

}