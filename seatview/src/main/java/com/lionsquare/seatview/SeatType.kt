package com.lionsquare.seatview

class SeatType  {
    object TYPE {
        val NOT_EXIST = 0
        val SELECTABLE = 1
        val UNSELECTABLE = 2
    }

    object MULTIPLETYPE {
        val NOTMULTIPLE = -1
        val LEFT = 0
        val CENTER = 1
        val RIGHT = 2
    }
}